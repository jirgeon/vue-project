import { request } from '@/request'

// 获取购物车列表数据
export function reqCartList() {
	return request.get(`/api/cart/cartList`)
}

// 切换商品选中状态
// skuID: 商品ID
// isChecked: 选中状态 1 选中 0 未选中
export function reqCheckCart(skuID, isChecked) {
	return request.get(`/api/cart/checkCart/${skuID}/${isChecked}`)
}

// 删除商品
export function reqDeleteCart(skuId) {
	return request.delete(`/api/cart/deleteCart/${skuId}`)
}

// 全选购物车 isChecked 1 选中 0 未选中，skuIdList 数组 代表修改的商品id列表
export function reqBatchCheckCart(isChecked, skuIdList) {
	return request.post(`/api/cart/batchCheckCart/${isChecked}`, skuIdList)
}

// 将选中的商品移除购物车 skuIdList 数组 代表删除的商品id列表
export function reqBatchDeleteCart(skuIdList) {
	return request.post(`/api/cart/batchDeleteCart`, skuIdList)
}
