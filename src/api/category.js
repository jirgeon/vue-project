import { request } from '@/request'

// REQ: 获取商品一级分类
export function reqGetCategory1() {
	return request.get(`/admin/product/getCategory1`)
}

// REQ: 获取商品二级分类
export function reqGetCategory2(category1Id) {
	return request.get(`/admin/product/getCategory2/${category1Id}`)
}

// REQ: 获取商品三级分类
export function reqGetCategory3(category2Id) {
	return request.get(`/admin/product/getCategory3/${category2Id}`)
}
