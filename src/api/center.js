import { request } from '@/request'

// 获取订单信息
export function reqCenterInfo(page, limit) {
	return request.get(`/api/order/auth/${page}/${limit}`)
}
