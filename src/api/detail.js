import { request } from '@/request'

// 请求商品详情数据
export function reqDetailInfo(skuId) {
	return request.get(`/api/item/${skuId}`)
}

// 添加到购物车(对已有物品进行数量改动)
export function reqAddOrUpdateCartNum(skuId, skuNum) {
	return request.post(`/api/cart/addToCart/${skuId}/${skuNum}`)
}
