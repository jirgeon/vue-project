import { requestMock } from '@/request'

export function reqBanner() {
	return requestMock.get(`/api/banner/list`)
}

export function reqFloor() {
	return requestMock.get(`/api/floor/list`)
}
