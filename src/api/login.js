import { request } from '@/request'

// 登录
export function reqLogin(userInfo) {
	return request.post(`/api/user/passport/login`, userInfo)
}

// 获取用户信息已验证 token 是否失效
export function reqGetUserInfo() {
	return request.get(`/api/user/passport/auth/getUserInfo`)
}

// 退出登录
export function reqLogout() {
	return request.get(`/api/user/passport/logout`)
}
