import { request } from '@/request'

// 获取交易页信息
export function reqPayInfo(orderId) {
	return request.get(`/api/payment/weixin/createNative/${orderId}`)
}

// 检测是否交易成功
export function reqQueryPayStatus(orderId) {
	return request.get(`/api/payment/weixin/queryPayStatus/${orderId}`)
}
