import { request } from '@/request'

// 发送验证码请求
export function reqSendCode(phone) {
	return request.get(`/api/user/passport/sendCode/${phone}`)
}

// 注册请求
export function reqRegister(userInfo) {
	return request.post(`/api/user/passport/register`, userInfo)
}
