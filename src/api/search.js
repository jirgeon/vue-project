import { request } from '@/request'

export function reqSearchInfo(searchParams) {
	return request.post(`/api/list`, searchParams)
}
