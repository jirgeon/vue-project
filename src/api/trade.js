import { request, requestMock } from '@/request'

// 获取交易页信息
export function reqTradeInfo() {
	return request.get(`/api/order/auth/trade`)
}

//请求用户地址
export const reqUserAddress = () => {
	return requestMock.get(`/api/address/list`)
}

// 提交订单
export function reqSubmitOrder(tradeNo, orderInfo) {
	return request.post(
		`/api/order/auth/submitOrder?tradeNo=${tradeNo}`,
		orderInfo
	)
}
