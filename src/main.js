import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import '@/mock'

import components from './main/components'
import directive from './main/directive'
import elementUI from './main/elementUI'
import lazyload from './main/lazyload'
import filters from './main/filters'
import veeValidate from './main/veeValidate'

Vue.use(components)
Vue.use(directive)
Vue.use(elementUI)
Vue.use(lazyload)
Vue.use(filters)
Vue.use(veeValidate)

Vue.config.productionTip = false

new Vue({
	render: h => h(App),
	router,
	store,
	beforeCreate() {
		Vue.prototype.$bus = this
	},
}).$mount('#app')
