import TypeNav from '@comp/TypeNav'
import Swiper from '@comp/Swiper'
import Pagination from '@comp/Pagination'
import 'swiper/css/swiper.min.css'

export default {
	install(Vue) {
		Vue.component('TypeNav', TypeNav) // 全局注册
		Vue.component('Swiper', Swiper)
		Vue.component('Pagination', Pagination)
	},
}
