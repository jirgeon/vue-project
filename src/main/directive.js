import loading from '@/static/images/loading.gif'

export default function (Vue) {
	// 定义懒加载指令
	Vue.directive('lazy1', {
		inserted(el, binging) {
			el.src = loading
			const io = new IntersectionObserver(([{ isIntersecting }]) => {
				if (isIntersecting) {
					setTimeout(() => {
						el.src = binging.value
					}, 500)
					io.unobserve(el)
				}
			})
			io.observe(el)
		},
	})
}
