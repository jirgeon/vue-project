export default function (Vue) {
	Vue.filter('price1', val => {
		return `￥${val.toFixed(2)}`
	})
	Vue.filter('price2', (val, num = 2) => {
		return `￥${val.toFixed(num)}`
	})
}
