import loading from '@/static/images/loading.gif'
import lazy from 'vue-lazyload'
export default function (Vue) {
	Vue.use(lazy, {
		loading,
	})
}
