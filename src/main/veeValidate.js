import Validate from 'vee-validate'
import zh_CN from 'vee-validate/dist/locale/zh_CN'

export default function (Vue) {
	Vue.use(Validate)

	Validate.Validator.localize('zh_CN', {
		messages: {
			...zh_CN.messages,
			is: n => {
				return n + '必须和密码相同'
			},
		},
		attributes: {
			code: '验证码',
			phone: '手机号',
			password: '密码',
			passwordAgain: '确认密码',
			agree: '协议',
		},
	})

	//扩展规则
	Validate.Validator.extend('agree', {
		//校验函数,当前函数返回true代表校验通过 返回false代表校验失败
		/* validate(value) {
      if (value === true) {
        return true;
      } else {
        return false;
      }
    } */
		validate: value => value,
		getMessage: n => {
			return n + '必须同意'
		},
	})
}
