import Mock from 'mockjs'
import banner from './banner.json'
import floor from './floor.json'
import address from './address.json'

Mock.mock('/api/banner/list', 'get', () => {
	return {
		code: 200,
		message: 'ok',
		data: banner,
	}
})

Mock.mock('/api/floor/list', 'get', () => {
	return {
		code: 200,
		message: 'ok',
		data: floor,
	}
})

Mock.mock('/api/address/list', 'get', () => {
	return {
		code: 200,
		message: 'ok',
		data: address,
	}
})
