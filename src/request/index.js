import request from './request'
import requestMock from './requestMock'

// 导出
export { request, requestMock }
