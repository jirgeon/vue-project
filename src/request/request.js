import axios from 'axios'
import { getUserTempId } from '@/utils/getuserTempId'
import store from '@/store'

const request = axios.create({
	baseURL: process.env.VUE_APP_API,
	timeout: 5000,
	headers: {},
})

// 请求拦截器
request.interceptors.request.use(
	config => {
		// 请求拦截器获取 token 并传递给后台
		const token = store.state.user.token
		config.headers.token = token
		config.headers.userTempId = getUserTempId()
		return config
	},
	error => {
		return Promise.reject(error)
	}
)

request.interceptors.response.use(
	response => {
		if (response.data.code !== 200) {
			return Promise.reject(response.data)
		}
		return response.data.data
	},
	error => {
		return Promise.reject(error)
	}
)

export default request
