import axios from 'axios'

const request = axios.create({
	baseURL: '',
	timeout: 5000,
	headers: {},
})

request.interceptors.request.use(
	config => {
		return config
	},
	error => {
		return Promise.reject(error)
	}
)

request.interceptors.response.use(
	response => {
		if (response.data.code !== 200) {
			return Promise.reject(response.data)
		}
		return response.data.data
	},
	error => {
		return Promise.reject(error)
	}
)

export default request
