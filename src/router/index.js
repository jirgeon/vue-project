import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import store from '@/store'

const oldPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(
	location,
	fn1 = () => {},
	fn2 = () => {}
) {
	return oldPush.call(this, location, fn1, fn2)
}

const oldReplace = VueRouter.prototype.replace
VueRouter.prototype.replace = function replace(
	location,
	fn1 = () => {},
	fn2 = () => {}
) {
	return oldReplace.call(this, location, fn1, fn2)
}

Vue.use(VueRouter)
const router = new VueRouter({
	mode: 'history',
	routes,
	// 滚动行为
	scrollBehavior() {
		return { x: 0, y: 0 }
	},
})

/**
 * 	路由鉴权：判断 token 是否存在
 *
 *	在：判断用户信息是否存在
 * 		用户信息存在则说明刚刚 token 请求过了
 * 		用户信息不存在，则发送用户信息请求（说明可能刷新或者第一次进来还没请求）
 * 			- 请求成功说明 token 合法有效放行
 * 			- 请求失败说名 token 失效或不合法将用户导航到登录页重新登录
 *
 * 	不在：
 * 		判断是否在白名单中，如果在放行
 * 		如果不在跳转到登录界面
 */

router.beforeEach(async (to, from, next) => {
	const token = store.state.user.token
	const nickName = store.state.user.userInfo.nickName
	console.log(1)
	if (token) {
		//判断要去的地方是不是登录页,如果是则直接导航到首页
		if (to.name === 'Login') {
			console.log(2)
			next('/')
		} else {
			console.log(nickName)
			//判断用户信息是否存在
			if (nickName) {
				console.log(3)
				next()
			} else {
				console.log(4)
				try {
					await store.dispatch('user/getUserInfo')
					next()
				} catch (e) {
					//请求用户信息失败 说明有问题
					//1. 清空用户信息和token
					store.commit('user/clean')
					//2.去登录
					next(`/login/${to.name}`)
				}
			}
		}
	} else {
		//判断当前要去的路由是否需要权限
		if (to.meta.isAuth) {
			console.log(5)
			next(`/login/${to.name}`)
		} else {
			console.log(6)
			next()
		}
	}
	next()
})

export default router
