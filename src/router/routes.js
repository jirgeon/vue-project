export default [
	// 默认路由
	{
		path: '/',
		redirect: '/home',
	},
	// 静态路由
	{
		path: '/home',
		name: 'Home',
		component: () => import('@/pages/Home'),
	},
	{
		path: '/search/:keyword?',
		name: 'Search',
		component: () => import('@/pages/Search'),
		props($route) {
			return {
				...$route.params,
				...$route.query,
			}
		},
	},
	{
		path: '/login/:to?',
		name: 'Login',
		component: () => import('@/pages/Login'),
		meta: {
			footerNotShow: true,
		},
	},
	{
		path: '/register',
		name: 'Register',
		component: () => import('@/pages/Register'),
		meta: {
			footerNotShow: true,
		},
	},
	{
		path: '/detail/:skuId',
		name: 'Detail',
		component: () => import('@/pages/Detail'),
	},
	{
		path: '/addCartSuccess',
		name: 'AddCartSuccess',
		component: () => import('@/pages/AddCartSuccess'),
	},
	{
		path: '/shopCart',
		name: 'ShopCart',
		component: () => import('@/pages/ShopCart'),
	},
	{
		path: '/trade',
		name: 'Trade',
		component: () => import('@/pages/Trade'),
		meta: {
			isAuth: true,
		},
		beforeEnter(to, from, next) {
			if (from.name === 'ShopCart') {
				next()
			} else {
				next('/shopCart')
			}
		},
	},
	{
		path: '/pay/:orderId',
		name: 'Pay',
		component: () => import('@/pages/Pay'),
		meta: {
			isAuth: true,
		},
		beforeEnter(to, from, next) {
			if (from.name === 'Trade') {
				next()
			} else {
				next('/shopCart')
			}
		},
	},
	{
		path: '/paySuccess',
		name: 'PaySuccess',
		component: () => import('@/pages/PaySuccess'),
		meta: {
			isAuth: true,
		},
		beforeEnter: (to, from, next) => {
			if (from.name === 'Pay') {
				next()
			} else {
				next('/shopCart')
			}
		},
	},
	{
		path: '/center',
		name: 'Center',
		component: () => import('@/pages/Center'),
		meta: {
			isAuth: true,
		},
		redirect: '/center/myOrder',
		children: [
			{
				path: 'myOrder',
				name: 'MyOrder',
				component: () => import('@/pages/Center/components/MyOrder'),
				meta: {
					isAuth: true,
				},
			},
			{
				path: 'teamOrder',
				name: 'TeamOrder',
				component: () => import('@/pages/Center/components/TeamOrder'),
				meta: {
					isAuth: true,
				},
			},
		],
	},
	{
		path: '/404',
		component: () => import('@/pages/404'),
	},

	// 任意路由
	{
		path: '/*',
		redirect: '/404',
	},
]
