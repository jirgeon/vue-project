import { reqLogin, reqGetUserInfo, reqLogout } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/token'

export default {
	namespaced: true,
	state: {
		token: getToken(),
		userInfo: {},
	},
	mutations: {
		setToken(state, payload) {
			state.token = payload.token
		},
		setUserInfo(state, payload) {
			state.userInfo = payload
		},
		clean(state, payload) {
			state.token = ''
			state.userInfo = {}
			removeToken()
		},
	},
	actions: {
		async getToken({ commit }, payload) {
			try {
				const result = await reqLogin(payload)
				commit('setToken', { token: result.token })

				// 将 token 永久化存储
				setToken(result.token)
			} catch (e) {
				return Promise.reject(e)
			}
		},
		async getUserInfo({ commit }) {
			try {
				const result = await reqGetUserInfo()
				commit('setUserInfo', result)
			} catch (e) {
				return Promise.reject(e)
			}
		},
		async getLogout({ commit }) {
			try {
				await reqLogout()
				// 退出登录清空
				commit('clean')
			} catch (e) {
				return Promise.reject(e)
			}
		},
	},
	getters: {},
}
