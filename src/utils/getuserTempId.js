import { nanoid } from 'nanoid'

const KEY = 'userTempId'
export function getUserTempId() {
	let userTempId = localStorage.getItem(KEY)
	// 如果没有那便创建并存储
	if (!userTempId) {
		userTempId = nanoid()
		localStorage.setItem(KEY, userTempId)
	}
	return userTempId
}
